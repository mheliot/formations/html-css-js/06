'use strict';

(function(document) {

    var addButton = document.querySelector('#addBtn')
    var addForm = document.querySelector('#addForm')
    var addModale = document.querySelector('#addModale')
    var cancelAddBtn = document.querySelector('#cancelAddBtn')

    function closeModale() {
        addModale.close()
    }

    function openModale() {
        addModale.showModal()
    }
    
    addButton.addEventListener('click', () => {
        openModale()
    })

    addForm.addEventListener('submit', event => {
        event.preventDefault()
        closeModale()
        alert('Article ajouté !')
        event.target.reset()
    })

    cancelAddBtn.addEventListener('click', () => {
        closeModale()
    })

}(document))